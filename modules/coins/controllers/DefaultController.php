<?php

namespace app\modules\coins\controllers;

use yii\web\Controller;

/**
 * Default controller for the `coins` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
