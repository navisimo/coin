<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'chipCount') ?>
    <?= $form->field($model, 'fieldsCount') ?>
    <?= Html::submitButton('Count', ['class' => 'btn btn-success']) ?>


<?php ActiveForm::end() ?>