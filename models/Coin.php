<?php
namespace app\models;


use yii\base\Model;


class Coin extends Model
{
    public $chipCount;
    public $fieldsCount;

    public function rules()
    {
        return [
/*            ['fieldsCount', 'required', 'when' => function($model, $attribute) {
                return true; //$model->chipCount < 0;
            }],*/

            ['chipCount', 'compare', 'compareAttribute' => 'fieldsCount', 'operator' => '<=', 'type' => 'number'],
            [['chipCount', 'fieldsCount'], 'required'],
            [['chipCount', 'fieldsCount'], 'integer', 'min' => 1, 'message' => 'Value should to be an integer and positive'],
 

        ];
    }

    public function getFileName()
    {
        return $this->fieldsCount.'_'.$this->chipCount.'.txt';
    }

    public function plunk()
    {
        $count = 0;
        $string = '';
        $min = pow(2, $this->chipCount) - 1;
        $max = pow(2, $this->fieldsCount) - pow(2, $this->fieldsCount - $this->chipCount);

        for ($i = $min; $i <= $max; $i++) {
            $bin = sprintf("%0{$this->fieldsCount}b", $i);

            if (substr_count($bin, '1') == $this->chipCount) {

                $string .= str_replace([0,1], ['.','$'], $bin).PHP_EOL;
                $count++;
            }
        }

        if ($count < 10){
            return 'менее 10 вариантов';
        } else {
            $text = 'количество строк: '.$count.PHP_EOL;
            $result = $text.$string;
            return $result;
        }
    }
}


?>