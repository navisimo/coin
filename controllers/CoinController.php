<?php

namespace app\controllers;

use yii\web\Controller;
use app\models\Coin;

class CoinController extends Controller
{

    public function actionIndex()
    {
        $model = new Coin();
        $result = null;

        if ($model->load($_POST) && $model->validate()) {

        return \Yii::$app->response->sendContentAsFile( $model->plunk(), $model->filename)->send();
        }
        return $this->render('index', ['model' => $model, 'result' => $result]);

    }

}
